/*
 *    Copyright 2014 - 2021 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

class DomActionSaver {
    static #sessions = {}
    #events = [];
    #currentSessionId;
    #mode;

    static createCss(showUi = true) {
        return new DomActionSaver(showUi, "css");
    }

    static createXpath(showUi = true) {
        return new DomActionSaver(showUi, "xpath");
    }

    constructor(showUi = true, mode = "xpath") {
        this.stop();
        this.start();

        let numberOfSessions = Object.keys(DomActionSaver.#sessions).length;
        let currentTimeStamp = Date.now();
        this.#currentSessionId = `div-domactionsave-${numberOfSessions}-${currentTimeStamp}`
        this.#mode = mode; //FIXME: Validate, to prevent script injection!
        let actionComponent = null;

        if (showUi) {
            actionComponent = this.#getIntegratedUiDivElement();
            DomActionSaver.#addComponentToBody(actionComponent);
        }

        DomActionSaver.#sessions[this.#currentSessionId] = {
            "id": this.#currentSessionId, "mode": mode, "instance": this, "ui": actionComponent
        }
    }

    /**
     * Method that create a DIV component with the integrated UI inside.
     * @returns {HTMLDivElement}
     */
    #getIntegratedUiDivElement() {
        let div = document.createElement("div");
        div.id = this.#currentSessionId;
        div.style.backgroundColor = "silver"
        div.style.border = "solid 3px black"
        div.style.display = "flex"
        div.style.flexDirection = "column"
        div.style.padding = "5px"
        div.innerHTML = this.#getIntegratedUiDivContent();
        return div;
    }

    /**
     * Method that generate the html for the UI.
     * @returns {string}
     */
    #getIntegratedUiDivContent() {
        return `
            <div style="display: flex; align-items: center; justify-content: flex-end">
                <span style="padding: 2px; color: dimgrey; cursor: pointer; font-weight: bolder" onclick="DomActionSaver.hide('${this.#currentSessionId}')">X</span>
            </div>
            <div style="display: flex; align-items: center">
                <span>DomActionSaver : ${this.#mode}</span>
            </div>
            <div style="display: flex; align-items: center;">
                <button onclick="DomActionSaver.stop('${this.#currentSessionId}')">Stop</button>
                <button onclick="DomActionSaver.restart('${this.#currentSessionId}')">Restart</button>
                <button onclick="DomActionSaver.clear('${this.#currentSessionId}')">Clear</button>
            </div>
            <textarea cols="100"></textarea>
        `;
    }

    #handleClickEventMethod = function (event) {
        let path = this.getPathFromModeAnd(event);

        if (!path || path.length === 0 || DomActionSaver.#isDomActionEventComponent(event)) {
            return;
        }

        this.#events.push({
            "type": event.type, "selector": path
        });

        this.#refresh();
    }.bind(this)

    #handleKeydownEventMethod = function (event) {
        let path = this.getPathFromModeAnd(event);

        if (!path || path.length === 0 || DomActionSaver.#isDomActionEventComponent(event)) {
            return;
        }

        this.#events.push({
            "type": event.type, "selector": path, "keys": {
                "ctrl": event.ctrlKey,
                "alt": event.altKey,
                "shift": event.shiftKey,
                "meta": event.metaKey,
                "key": event.key,
                "code": event.code
            }
        });

        this.#refresh();
    }.bind(this)

    /**
     * Method to extract a path from the Mode and an Event.
     * @param event
     * @returns {string}
     */
    getPathFromModeAnd(event) {
        if (this.#mode === "xpath") {
            return DomActionSaver.#generateXpathSelectorFrom(event);
        } else if (this.#mode === "css") {
            return DomActionSaver.#generateCssSelectorFrom(event);
        } else {
            return undefined;
        }
    }

    /**
     * Method to update the text contained in the text area
     */
    #refresh() {
        this.setTextAreaValue(this.toJson());
    }

    /**
     * Method to set the text into the text area
     * @param textContent
     */
    setTextAreaValue(textContent) {
        let element = document.querySelector(`#${this.#currentSessionId} > textarea`);

        if (!element) {
            return;
        }

        if (!textContent) {
            element.textContent = ""
        } else {
            element.textContent = textContent
        }
    }

    /**
     * Method to add a component at the beginning of the body.
     * @param component
     */
    static #addComponentToBody(component) {
        let body = document.getElementsByTagName("body")[0];
        body.prepend(component);
    }

    /**
     * Method to check if the current name of the element is a dom element.
     * @param name
     * @returns {boolean}
     */
    static #isDomElement(name) {
        switch (name.toLowerCase()) {
            case "body" :
            case "html" :
                return true;
            default:
                return false;
        }
    }

    /**
     * Method that generates a xpath selector from an event.
     * @param event The event
     * @returns {string|null}
     */
    static #generateXpathSelectorFrom(event) {
        if (!event) {
            return null;
        }

        let target = event.target;
        let elementId = target.id;

        if (elementId) {
            return `//*[@id="${elementId}"]`;
        } else {
            let parentChain = DomActionSaver.#buildParentChain(target);
            return this.#buildXpathFromChain(parentChain);
        }
    }

    /**
     * Method that generates a css selector from an event.
     * @param event The event
     * @returns {string|null}
     */
    static #generateCssSelectorFrom(event) {
        if (!event) {
            return null;
        }

        let target = event.target;
        let elementId = target.id;

        if (elementId) { // With id
            return `#${elementId}`;
        } else {
            let classList = target.classList;
            let localName = target.localName;

            if (classList && classList.length > 0) { // With classes
                let selector = localName;

                for (const currentClass of classList) {
                    selector += `.${currentClass}`;
                }

                return selector;
            } else {
                return null; //TODO: Implement
            }
        }
    }

    /**
     * Method that generates a xpath selector from a chain.
     * @param parentChain
     * @returns {string}
     */
    static #buildXpathFromChain(parentChain) {
        let xpath = "";
        let isFirstParentAdded = false;
        let numberOfItemInTheChain = parentChain.length;

        // Build the xpath, from the parents
        for (let i = 0; i < numberOfItemInTheChain; i++) {
            const parentElement = parentChain[i];

            if (i + 1 === numberOfItemInTheChain) {
                break;
            }

            const childElement = parentChain[i + 1];
            let parentNodeName = parentElement.nodeName.toLowerCase();
            let childNodeName = childElement.nodeName.toLowerCase();

            if (!isFirstParentAdded && numberOfItemInTheChain > 1) {
                xpath += `/${parentNodeName}`;
                isFirstParentAdded = true;
            }

            if (parentElement.childElementCount > 1) {
                let childNodes = Array.from(parentElement.childNodes);

                let filteredChildNode = childNodes.filter(item => {
                    return item.nodeName.toLowerCase() === childNodeName && !DomActionSaver.#sessions[item.id];
                });

                if (filteredChildNode.length > 1 && !this.#isDomElement(childNodeName)) {
                    let indexOfElement = filteredChildNode.indexOf(childElement);
                    if (indexOfElement > -1) {
                        xpath += `/${childNodeName}[${indexOfElement + 1}]`;
                    }
                } else {
                    xpath += `/${childNodeName}`;
                }
            } else {
                xpath += `/${childNodeName}`;
            }
        }

        return xpath;
    }

    /**
     * Method to check if the session exists
     * @param sessionId
     */
    static #isSessionExist(sessionId) {
        if (!sessionId) {
            return false;
        }

        let session = DomActionSaver.#sessions[sessionId];
        return session !== undefined && session !== null;
    }

    /**
     * Method to fetch the session, null if not present
     * @param sessionId
     */
    static #getSessionFromId(sessionId) {
        if (!sessionId) {
            return null;
        }

        return DomActionSaver.#sessions[sessionId];
    }

    /**
     * Method to fetch the session's instance, null if not present
     * @param sessionId
     */
    static #getInstanceFromId(sessionId) {
        return this.#getSessionFromId(sessionId)?.instance;
    }

    /**
     * Method that build a chain from the parents.
     * @param target
     * @returns {*[]}
     */
    static #buildParentChain(target) {
        let itemChain = [];
        let parent = target.parentElement;

        while (parent != null) {
            itemChain.push(parent)
            parent = parent.parentElement;
        }

        itemChain.reverse();
        itemChain.push(target);
        return itemChain;
    }

    /**
     * Method to check if the element is contained inside the main div of the saver.
     * @param event
     * @returns {boolean}
     */
    static #isDomActionEventComponent(event) {
        let parent = event.target.parentElement;

        while (parent != null) {
            if (DomActionSaver.#isSessionExist(parent.id)) {
                return true;
            }
            parent = parent.parentElement;
        }

        return false;
    }

    /**
     * Method to restart the events
     */
    restart() {
        this.stop();
        this.start();
    }

    static restart(sessionId) {
        this.#getInstanceFromId(sessionId)?.restart();
    }

    /**
     * Method to stop the events
     */
    stop() {
        window.removeEventListener("click", this.#handleClickEventMethod);
        window.removeEventListener("keydown", this.#handleKeydownEventMethod);
        this.clear();
    }

    static stop(sessionId) {
        this.#getInstanceFromId(sessionId)?.stop();
    }

    /**
     * Method to start the events
     */
    start() {
        this.stop();
        window.addEventListener("click", this.#handleClickEventMethod);
        window.addEventListener("keydown", this.#handleKeydownEventMethod);
    }

    /**
     * Method to clear the results
     */
    clear() {
        this.#events.splice(0, this.#events.length)
        this.setTextAreaValue("");
    }

    static clear(sessionId) {
        this.#getInstanceFromId(sessionId)?.clear();
    }

    /**
     * Method to print the results to the console
     */
    toJson() {
        return JSON.stringify(this.#events, null, 4)
    }

    /**
     * Method to remove the UI
     */
    hide() {
        let session = DomActionSaver.#getSessionFromId(this.#currentSessionId);

        if (!session) {
            return;
        }

        session?.ui?.remove();
        session.ui = null;
    }

    static hide(sessionId) {
        this.#getInstanceFromId(sessionId)?.hide();
    }

    /**
     * Method to add the UI
     */
    show() {
        let session = DomActionSaver.#getSessionFromId(this.#currentSessionId);

        if (!session || session.ui) {
            return;
        }

        let ui = this.#getIntegratedUiDivElement();
        session.ui = ui;
        DomActionSaver.#addComponentToBody(ui);

        this.#refresh();
    }
}