# dom-action-event-saver

`dom-action-event-saver` is a small library that keep the user actions into memory and allow an extraction in a json format.

This library can be useful when using automation frameworks.

For the moment, only those actions are supported
* click
* keydown

**Example of events**

```json
[
  {
    "type": "click",
    "selector": "/html/body/div/div/div[2]/div[1]/div/div[7]/div/div/div[1]/div[2]/div[2]/button"
  },
  {
    "type": "keydown",
    "selector": "//*[@id=\"super-div\"]",
    "keys": {
      "alt": false,
      "code": "KeyY",
      "ctrl": true,
      "key": "Y",
      "meta": false,
      "shift": true
    }
  }
]
```

## To use

1. Import `DomActionSaver.js` into your application
2. Create a new instance with either the constructor `new DomActionSaver()` or the static method `DomActionSaver.createCss` / `DomActionSaver.createXpath`

By default, the integrated UI is enabled; to disable the UI, pass `false` to the constructor.

### From the console

You can use the provided methods to interact with the core.

* `instance.toJson()`
* `instance.stop()`
* `instance.restart()`
* `instance.start()`